import csv, os

##fonction verifiant que le fichier existe, est un fichier, de type csv
def verif_fichiercsv(path):
    if os.path.exists(path) and os.path.isfile(path) and path.split('.')[len(path.split('.')) - 1] == "csv":
        return True
    else:
        return False
##fonction permettant l'ouverture et la lecture d'un fichier, elle prend en parametre un caractère, afin de separer du texte
def ouverture_csv(path, delimiteur):
    patern = {}                                             
    list_data = []                                          
    names = []                                              
    with open(path) as csvfile:
        reader = csv.reader(csvfile, delimiter=delimiteur)  
        i = 0
        for row in reader:
            if len(patern) == 0:
                for name in row:
                    patern[name] = ""
                    names.append(name)
                list_data.append(patern)                    
            else:
                list_data.append({})
                for indice in range(len(row)):
                    list_data[i][names[indice]] = row[indice]
            i = i + 1
    return list_data


def touch(path):
    with open(path, 'a'):
        os.utime(path, None)

##fonction permettant l'écriture dans fichier
def ecriture_csv(path, data, delimiteur):
    if (verif_fichiercsv == False):
        touch(path)
    with open(path, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=delimiteur)
        patern_write = False
        for row in data:
            out=[]
            if patern_write == False:
                for name in row:
                    out.append(name)
                patern_write = True
            else:
                for name in row:
                    out.append(row[name])
            writer.writerow(out)