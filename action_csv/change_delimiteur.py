##Prend en paramètre une chaine de caracrtére, et deux caractère : delim_init, delim_final
def change_delimiteur(string_in, delim_init, delim_final): 
    string_out = ""                                         ##chaine vide 
    for i in range(len(string_in)):                         ##boucle parcourant la chaine
        if string_in[i] == delim_init:                      ##Si le caractère est delim_init
            string_out += delim_final                       ##ajoute le caractère delim_final a la chaine
        else:
            string_out += string_in[i]                      ##Sinon ajoute le caractère
    return string_out
##Retourne la chaine de caractère modifié