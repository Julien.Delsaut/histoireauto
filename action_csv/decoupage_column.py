import datetime
'''
    Fonction split_column
    %       
            
            ensuite pour chaque clé présente dans le patern de convertion on
            regarde quelle est l'action à efffectuer et on place la traitement de la
            donnée dans le dictionnaire à la même clé que celle du patern de convertion
            (On la supplante si elle est déjà présente).
            Les 'actions' actuellement implémentées sont celle de convertion, celle de
            découpe et celle de formatage d'une date.
            La convertion replace la donnée à une autre clé.
            La découpe récupère l'index d'un colonne suite à son découpage selon un
            caractère de découpe.
            Le formatage d'une date récupère la date, ainsi que son format initial et
            le format final et envoie le tout à la fonction date_format.
'''
##Cette fonction convertit des données d'un dictionnaire, selon le patern de convertion passé en argument.
def decoupage_column(data_in, patern_convertion):
    data_out = data_in                                           ##copie du dictionnaire passe en argument
    for key in patern_convertion.keys():                            ##regarde l'action a effectuer pour chaque cle
        for indice in range(len(patern_convertion[key])):
            ## La convertion replace la donnée à une autre clé
            if patern_convertion[key][indice]['action']=="convertir":
                data_out[key] = data_out[patern_convertion[key][indice]['data']]
            ##La découpe récupère l'index d'un colonne suite à son découpage selon un caractère de découpe.
            elif patern_convertion[key][indice]['action']=="decoupe":
                data_out[key] = data_out[patern_convertion[key][indice]['data']].split(patern_convertion[key][indice]['delimiter'])[patern_convertion[key][indice]['index']]
            #Le formatage d'une date récupère la date, ainsi que son format initial et le format final et envoie le tout à la fonction date_format.
            elif patern_convertion[key][indice]['action']=="date_format":
                data_out[key] = date_format(data_out[patern_convertion[key][indice]['data']], patern_convertion[key][indice]['format_init'], patern_convertion[key][indice]['format_final'])
    return data_out

##Permet le changement de format de la date
def date_format(date, format_init, format_final):
    return datetime.datetime.strptime(date, format_init).strftime(format_final)
