##Cette fonction vérifie que le dictionnaire data_in correspond bien au patern passé en argument
def reorder_csv(data_in, patern):
    if patern.keys() == data_in.keys():
        data_out = data_in
    else:
        data_temp = data_in             ##creation d'une copie de data_in
        for key in patern.keys():       ##ajout des éléments manquant au patern en les initiant avec une chaine de caractère vide
            if key not in data_temp:    
                data_temp[key]=""
        data_out={}
        for key in patern.keys():       ##récupère les données dans la copie et les réordonne dans le même ordre que le patern
            data_out[key]=data_temp[key]
    return data_out
    ## retourne le dictionnaire data_out