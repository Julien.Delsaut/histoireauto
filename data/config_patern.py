delimiteur_initial = "|"
delimiteur_final = ";"
patern = {
    "adresse_titulaire":"",
    "nom":"",
    "prenom":"",
    "immatriculation":"",
    "date_immatriculation":"",
    "vin":"",
    "marque":"",
    "denomination_commerciale":"",
    "couleur":"",
    "carrosserie":"",
    "categorie":"",
    "cylindre":"",
    "energie":"",
    "places":"",
    "poids":"",
    "puissance":"",
    "type":"",
    "variante":"",
    "version":""    
}
patern_convertion = {
    'adresse_titulaire':[{'action':"convertir",'data':'address'}],
    'cylindre':[{'action':"convertir",'data':'cylindree'}],
    'date_immatriculation':[{'action':"convertir",'data':'date_immat'},{'action':"date_format",'data':'date_immatriculation','format_init':'%Y-%m-%d','format_final':'%d/%m/%Y'}],
    'denomination_commerciale':[{'action':"convertir",'data':'denomination'}],
    'energie':[{'action':"convertir",'data':'energy'}],
    'prenom':[{'action':"convertir",'data':'firstname'}],
    'nom':[{'action':"convertir",'data':'name'}],
    'immatriculation':[{'action':"convertir",'data':'immat'}],
    'type':[{'action':"decoupe",'delimiter':',','index':0,'data':'type_variante_version'}],
    'variante':[{'action':"decoupe",'delimiter':',','index':1,'data':'type_variante_version'}],
    'version':[{'action':"decoupe",'delimiter':',','index':2,'data':'type_variante_version'}]
}