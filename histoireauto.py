import argparse
from data.config_patern import delimiteur_initial, delimiteur_final, patern, patern_convertion
from action_csv.reorder_csv import reorder_csv
from action_csv.decoupage_column import decoupage_column
from action_csv.change_delimiteur import change_delimiteur
from action_csv.action_fichier import verif_fichiercsv, ouverture_csv, ecriture_csv

parser = argparse.ArgumentParser(description='example')
parser.add_argument("file", help="csv file to convert")
parser.add_argument("-output", help="name for output (Default : 'a.csv')")
parser.add_argument("-delimiteur", help="Delimiteur used (Default : ';')")
parser.add_argument("--same_file", help="Option for rewrite original file (True or False)")
parser.add_argument("--change_delimiteur_only", help="Option for change delimiteur only (True or False)")
args = parser.parse_args()

if (args.delimiteur != None):
    final_delimiter = args.delimiteur

if (verif_fichiercsv(args.file) == True):
    if(args.change_delimiteur_only != 'True'):
        data_in = ouverture_csv(args.file, delimiteur_initial)
        data_out = []
        first_row = True
        for row in data_in:
            if first_row == True:
                data = row
                first_row = False
            else:
                data = decoupage_column(row, patern_convertion)
            data = reorder_csv(data, patern)
            data_out.append(data)
        if(args.output != 'True'):
            if(args.same_file != 'True'):
                path_output = "auto_modifie.csv"
            else:
                path_output = args.file
        else:
            path_output = args.output
        ecriture_csv(path_output, data_out, delimiteur_final)
    else:
        data_in = open(args.file)
        data_out = []
        for row in data_in:
            data_out.append(change_delimiteur(row, delimiteur_initial, delimiteur_final))
        file_write = open(args.file, "w")
        for row in data_out:
            file_write.write(row)
else:
    print("Fichier introuvable")