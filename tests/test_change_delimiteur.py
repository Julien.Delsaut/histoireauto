import unittest
from action_csv.change_delimiteur import change_delimiteur


class TestChangeDelimiter(unittest.TestCase): #Les noms de classe commencent impérativement par une majuscule
    def test_change_delimiter(self):
        doc = "1,2,3"
        self.assertEqual("1;2;3", change_delimiteur(doc, ',', ';'))
