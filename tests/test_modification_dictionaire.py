import unittest
from action_csv.reorder_csv import reorder_csv
from action_csv.decoupage_column import decoupage_column, date_format

class TestReworkDictionary(unittest.TestCase):
    
    def test_reorder(self):
        patern = {'exemple':"",'lautre_exemple':""}
        exemple = {'1':"", 'lautre_exemple':3}
        verif = {'exemple':"",'lautre_exemple':3}
        self.assertEqual(verif, reorder_csv(exemple, patern))

    def test_decoupe(self):
        patern = {
            'exemple':[{'action':"decoupe",'delimiter':',','index':0,'data':'1'}],
            'example':[{'action':"decoupe",'delimiter':',','index':1,'data':'1'}],
            'test':[{'action':'convertir','data':'teste'}],
            'date':[{'action':"convertir",'data':'dat'},{'action':"date_format",'data':'date','format_init':'%d/%m/%Y','format_final':'%Y-%m-%d'}]
        }
        example = {'1':"3,2", 'teste':3, 'dat':'01/04/2000'}
        verif = {'1':"3,2", 'teste':3,'dat':'01/04/2000','exemple':"3",'example':"2","test":3,"date":"2000-04-01"}
        self.assertEqual(verif, decoupage_column(example, patern))

    def test_date_format(self):
        self.assertEqual('01/04/2000', date_format('2000-04-01', '%Y-%m-%d', '%d/%m/%Y'))
        self.assertEqual('2000-04-01', date_format('01/04/2000', '%d/%m/%Y', '%Y-%m-%d'))

    def test_split_and_rework(self):
        patern_convert = {
            'exemple':[{'action':"decoupe",'delimiter':',','index':0,'data':'1'}],
            'example':[{'action':"decoupe",'delimiter':',','index':1,'data':'1'}],
            'test':[{'action':'convertir','data':'teste'}],
            'date':[{'action':"convertir",'data':'dat'},{'action':"date_format",'data':'date','format_init':'%d/%m/%Y','format_final':'%Y-%m-%d'}]
        }
        patern = {'exemple':"",'example':"",'test':"", 'date':""}
        example = {'1':"3,2", 'teste':3, 'dat':'01/04/2000'}
        verif = {'exemple':"3",'example':"2","test":3,"date":"2000-04-01"}
        self.assertEqual(verif, reorder_csv(decoupage_column(example, patern_convert), patern))